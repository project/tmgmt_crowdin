<?php

namespace Drupal\tmgmt_crowdin\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt_crowdin\Plugin\tmgmt\Translator\CrowdinTranslator;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CrowdinWebhookController extends ControllerBase
{

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function create(ContainerInterface $container): self
    {
        return new static($container->get('logger.factory')->get('tmgmt_crowdin'));
    }

    public function process(Request $request): JsonResponse
    {
        $response = ['success' => false, 'translations_updated' => false];

        $data = Json::decode($request->getContent());
        $project_id = $data['project_id'] ? $data['project_id'] : $data['file']['project']['id'];
        $file_id = $data['file_id'] ? $data['file_id'] : $data['file']['id'];
        $target_language = $data['language'] ? $data['language'] : $data['targetLanguage']['id'];

        $file_path = explode('/', $data['file']['path']);

        [$job_id, $job_item_id] = sscanf(end($file_path), CrowdinTranslator::FORMAT_FILE_NAME);

        if (!$job_id && !$job_item_id) {
            $response['message'] = 'Invalid file path. Job id and job item id are not found.';
            return new JsonResponse($response, Response::HTTP_BAD_REQUEST);
        }

        /** @var JobInterface $job */
        $job = Job::load($job_id);
        /** @var JobItemInterface $job_item */
        $job_item = JobItem::load($job_item_id);

        if ($job->isAborted() || $job_item->isAborted()) {
            $message = 'The job (' . $job->id() . ') or job item (' . $job_item->id() . ') is not active.';

            $this->logger->warning($message);
            $job->addMessage($message, 'warning');

            $response['message'] = $message;

            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }

        /** @var TranslatorInterface $translator */
        $translator = $job->getTranslator();
        /** @var CrowdinTranslator $crowdin_translator */
        $crowdin_translator = $translator->getPlugin();

        $webhook_id_by_project_id = unserialize($crowdin_translator->getCrowdinData('webhook_id_by_project_id'));

        if (!array_key_exists($project_id, $webhook_id_by_project_id)) {
            $response['message'] = 'Webhook id is not found for project id ' . $project_id;
            return new JsonResponse($response, Response::HTTP_BAD_REQUEST);
        }

        $response['success'] = true;
        $project = $crowdin_translator->getProject($translator, $project_id);
        $export_approved_only = $project['data']['exportApprovedOnly'] ?? (bool)$project['data']['exportWithMinApprovalsCount'];

        if ($export_approved_only && $data['event'] !== CrowdinTranslator::FILE_APPROVED_EVENT) {
            return new JsonResponse($response, Response::HTTP_OK);
        }

        try {
            if ($crowdin_translator->updateTranslation($job_item, $project, $file_id, $target_language)) {
                return new JsonResponse(['success' => true, 'translations_updated' => true], Response::HTTP_OK);
            }
        } catch (TMGMTException $e) {
            $job_item->addMessage($e->getMessage());
        } catch (\Exception $e) {
            watchdog_exception('tmgmt_crowdin', $e);
            return new JsonResponse(NULL, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(['success' => true, 'translations_updated' => false], Response::HTTP_OK);
    }

}
