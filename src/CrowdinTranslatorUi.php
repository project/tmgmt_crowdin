<?php

namespace Drupal\tmgmt_crowdin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\tmgmt_crowdin\Plugin\tmgmt\Translator\CrowdinTranslator;

class CrowdinTranslatorUi extends TranslatorPluginUiBase
{
    use StringTranslationTrait;

    public function buildConfigurationForm(array $form, FormStateInterface $form_state): array
    {
        $form = parent::buildConfigurationForm($form, $form_state);

        /** @var TranslatorInterface $translator */
        $translator = $form_state->getFormObject()->getEntity();

        $form['personal_token'] = [
            '#type' => 'textfield',
            '#required' => true,
            '#title' => $this->t('Personal Access Token'),
            '#default_value' => $translator->getSetting('personal_token'),
            '#description' => $this->t(
                'Please enter your Personal Access Token or check how to create one <a href="@url">Crowdin Support</a> or <a href="@url_enterprise">Crowdin Enterprise Support</a>',
                [
                    '@url' => 'https://support.crowdin.com/account-settings/#api',
                    '@url_enterprise' => 'https://support.crowdin.com/enterprise/personal-access-tokens/',
                ]
            )
        ];

        $form['project_id'] = [
            '#type' => 'textfield',
            '#required' => true,
            '#title' => $this->t('Project Id'),
            '#default_value' => $translator->getSetting('project_id'),
        ];

        $form['domain'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Enterprise Domain'),
            '#default_value' => $translator->getSetting('domain'),
            '#description' => $this->t('Please enter your Enterprise Domain'),
        ];

        $form += parent::addConnectButton();

        return $form;
    }

    public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
        $settings['templates_wrapper']['description'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Job Description'),
            '#rows' => '2',
            '#description' => $this->t('The field allows users to provide instructions and information for a translation job item, helping translators with specific requirements and content.'),
        ];

        return $settings;
    }

    public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void
    {
        parent::validateConfigurationForm($form, $form_state);

        /** @var TranslatorInterface $translator */
        $translator = $form_state->getFormObject()->getEntity();
        /** @var CrowdinTranslator $crowdinTranslator */
        $crowdinTranslator = $translator->getPlugin();

        $projectId = $translator->getSetting('project_id');

        if (
            !$translator->getSetting('personal_token')
            || !$projectId
            || !$crowdinTranslator->getProject($translator, $projectId)
        ) {
            $form_state->setError($form['plugin_wrapper']['settings']['personal_token'], t('Personal Access Token is not valid.'));
        }
    }

    public function checkoutInfo(JobInterface $job): array
    {
        $form = [];

        $form['actions']['description'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Job Description'),
            '#default_value' => $job->settings->templates_wrapper['description'],
            '#rows' => '2',
            '#disabled' => true,
            '#weight' => -20,
        ];

        if ($job->isActive()) {
            $form['actions']['pull'] = [
                '#type' => 'submit',
                '#value' => $this->t('Fetch translations'),
                '#submit' => [[$this, 'submitFetchTranslations']],
                '#weight' => -10,
            ];

            $form['actions']['update'] = [
                '#type' => 'submit',
                '#value' => $this->t('Update Source Texts'),
                '#submit' => [[$this, 'updateSourceData']],
                '#weight' => -10,
            ];

            $form['title'] = [
                '#type' => 'label',
                '#title' => $this->t('If you’ve made changes to your source texts at Drupal, click on Update Source Texts to instantly sync changes with Crowdin.'),
            ];
        }

        return $form;
    }

    public function submitFetchTranslations(array $form, FormStateInterface $form_state): void
    {
        /** @var \Drupal\tmgmt\Entity\Job $job */
        $job = $form_state->getFormObject()->getEntity();

        /** @var CrowdinTranslator $crowdin_plugin */
        $crowdin_plugin = $job->getTranslator()->getPlugin();
        $crowdin_plugin->fetchTranslations($job);
    }

    public function updateSourceData(array $form, FormStateInterface $form_state): void
    {
        /** @var \Drupal\tmgmt\Entity\Job $job */
        $job = $form_state->getFormObject()->getEntity();

        /** @var CrowdinTranslator $crowdinTranslator */
        $crowdinTranslator = $job->getTranslator()->getPlugin();
        $crowdinTranslator->requestJobItemsTranslation($job->getItems());
    }
}
