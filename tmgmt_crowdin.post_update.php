<?php

/**
 * @file
 * Install, update, and uninstall functions for the Crowdin Translator module.
 */

use Drupal\Core\Url;

/**
 * Removes crowdin.settings.
 */
function tmgmt_crowdin_post_update_delete_crowdin_settings() {
  $legacy_config = \Drupal::configFactory()->getEditable('crowdin.settings');
  if ($legacy_config->isNew()) {
    // The legacy config doesn't exist, so nothing to do.
    return;
  }

  // Check that the legacy config values match the "new" ones. If not, don't
  // delete the legacy config (to avoid data loss) and provide a message.
  $config = \Drupal::config('tmgmt.translator.crowdin');
  foreach (['personal_token', 'project_id', 'domain'] as $key) {
    // Make sure we're comparing non-overridden values. The legacy config is
    // mutable for deletion so will already return non-overridden values; the
    // new config is immutable so use getOriginal().
    if ($legacy_config->get($key) !== $config->getOriginal("settings.$key", FALSE)) {
      $parameters = ['tmgmt_translator' => 'crowdin'];
      $options = ['absolute' => TRUE];
      $url = Url::fromRoute('entity.tmgmt_translator.edit_form', $parameters, $options);
      $context = ['@link' => $url->toString()];
      return t("The configuration in 'crowdin.settings' differs from 'tmgmt.translator.crowdin'. Go to @link and ensure the values are correct and then delete 'crowdin.settings'. See https://www.drupal.org/project/tmgmt_crowdin/releases/8.x-1.5 for more details.", $context);
    }
  }

  $legacy_config->delete();
  return t("Deleted configuration entity 'crowdin.settings'.");
}
